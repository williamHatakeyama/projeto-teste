package br.com.springboot.curso_jdev_teste.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.springboot.curso_jdev_teste.model.Pessoa;

@Component
@Scope("singleton")
public class PessoaDAO {

	private List<Pessoa> pessoasList;

	public PessoaDAO() {
		pessoasList = new ArrayList<Pessoa>();
	}

	public void salvarPessoa(Pessoa pessoa) {
		pessoasList.add(pessoa);
	}

	public List<Pessoa> retornarPessoas() {
		return pessoasList;
	}

	public void updatePessoas(Pessoa pessoa) {
		pessoasList.stream().forEach(p -> {
			if (p.getCpf().equals(pessoa.getCpf())) {
				p.setNome(pessoa.getNome());
			}
		});
	}

	public void removerPessoa(String cpf) {
		pessoasList.removeIf(p -> p.getCpf().equals(cpf));
	}

	public Pessoa buscarPorCPF(String cpf) {
		return pessoasList.stream().filter(p -> p.getCpf().equals(cpf)).findFirst().get();
	}

}

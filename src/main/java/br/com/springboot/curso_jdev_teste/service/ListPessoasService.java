package br.com.springboot.curso_jdev_teste.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.springboot.curso_jdev_teste.dao.PessoaDAO;
import br.com.springboot.curso_jdev_teste.model.Pessoa;

@Service
public class ListPessoasService {
	
	
	@Autowired
	private PessoaDAO pessoaDAO;
	
	public List<Pessoa> execute() {
		return pessoaDAO.retornarPessoas();
		
	}

}

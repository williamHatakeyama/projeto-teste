package br.com.springboot.curso_jdev_teste.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import br.com.springboot.curso_jdev_teste.dao.PessoaDAO;
import br.com.springboot.curso_jdev_teste.model.Pessoa;

@Service
public class CreatePessoaService {
	
	@Autowired
	private PessoaDAO pessoaDAO;
	
	
	public Pessoa execute(Pessoa pessoa) {
		pessoaDAO.salvarPessoa(pessoa);
		return pessoa;
	}
}

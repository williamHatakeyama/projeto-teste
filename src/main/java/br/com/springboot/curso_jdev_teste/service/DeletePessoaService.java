package br.com.springboot.curso_jdev_teste.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.springboot.curso_jdev_teste.dao.PessoaDAO;
import br.com.springboot.curso_jdev_teste.model.Pessoa;

@Service
public class DeletePessoaService {
	
	@Autowired
	private PessoaDAO pessoaDAO;
	
	public String execute(String cpf) {
		pessoaDAO.removerPessoa(cpf);
		return cpf;
	}
}

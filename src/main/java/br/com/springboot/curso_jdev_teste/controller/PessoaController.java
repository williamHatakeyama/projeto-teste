package br.com.springboot.curso_jdev_teste.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.springboot.curso_jdev_teste.model.Pessoa;
import br.com.springboot.curso_jdev_teste.service.CreatePessoaService;
import br.com.springboot.curso_jdev_teste.service.DeletePessoaService;
import br.com.springboot.curso_jdev_teste.service.ListPessoasService;
import br.com.springboot.curso_jdev_teste.service.UpdatePessoaService;

@RestController
public class PessoaController {
	
	@Autowired
	private CreatePessoaService creatPessoaService;
	
	@Autowired
	private ListPessoasService listPessoasService;
	
	@Autowired
	private DeletePessoaService deletePessoaService;
	
	@Autowired
	private UpdatePessoaService updatePessoaService;
	
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public String greetingText(@PathVariable String name) {
        return "Hello " + name + "!";
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Pessoa>> retornarLista() {
        return ResponseEntity.ok(listPessoasService.execute());
    } 
    
    @RequestMapping(value = "/salvarpessoa", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Pessoa> createPessoa(@RequestBody Pessoa pessoa) {
        return ResponseEntity.ok(creatPessoaService.execute(pessoa));
    }
    
    @RequestMapping(value = "/delete/{cpf}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> removePessoa(@PathVariable String cpf) {
        return ResponseEntity.ok(deletePessoaService.execute(cpf));
    }
    
    @RequestMapping(value = "/update/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> updatePessoa(@RequestBody Pessoa pessoa) {
        return ResponseEntity.ok(updatePessoaService.execute(pessoa));
    }
    
    
    
    

}
